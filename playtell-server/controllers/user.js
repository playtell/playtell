import { UserModel, userSchema, mongoose } from '../models/user.js';

const UserController = {
  Create: async (name, pass, email) => {
    if (name && pass && email) {
      let checkUniqueUsername = async (name) => {
        let returnVal = await UserController.Read(name, null, null);
        return returnVal === null ? true : false;
      }
      let checkUniqueEmail = async (email) => {
        let returnVal = await UserController.Read(null, null, email);
        return returnVal === null ? true : false;
      }

      let isUsernameUnique = await checkUniqueUsername(name);
      let isEmailUnique = await checkUniqueEmail(email);

      if (isUsernameUnique && isEmailUnique) {
        let returnUser =  new UserModel({username: name, password: userSchema.methods.encryptPass(pass), email});
        returnUser.save();
        return returnUser;
      } else {
        return false;
      }
    } else {
      return false;
    }
  },
  Read: async (name=null, pass=null, email=null) => {
    if (name && pass && (!email || typeof email === 'undefined')) {
      // Read USER by USERNAME & PASSWORD
      console.log('Read USER by USERNAME & PASSWORD');
      return UserModel.findOne({ username: name, password: userSchema.methods.encryptPass(pass) }, function (err, user) {});
    } else if (email && pass && (!name || typeof name === 'undefined')) {
      // Read USER by EMAIL & PASSWORD
      console.log('Read USER by EMAIL & PASSWORD');
      return UserModel.findOne({ email: email, password: userSchema.methods.encryptPass(pass) }, function (err, user) {});
    } else if (email && (!name || typeof name === 'undefined') && (!pass || typeof pass === 'undefined') ) {
      // Read USER by EMAIL
      console.log('Read USER by EMAIL');
      return UserModel.findOne({ email: email }, function (err, user) {});
    } else if (name && (!pass || typeof pass === 'undefined') && (!email || typeof email === 'undefined')) {
      // Read USER by USERNAME
      console.log('Read USER by USERNAME');
      return UserModel.findOne({ username: name }, function (err, user) {});
    } else {
      // Bad call
      return false;
    }
  },
  Update: () => {},
  Delete: () => {}
};

export { UserController };

