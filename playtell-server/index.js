import express from 'express';
import { UserController } from './controllers/user.js';

var userRequestThrottle = {};

var App = express();

App.listen(3000, () => {
  console.log('Listening on 3000');
});

App.get('/createuser', (req, res) => {
  if (typeof req.query.user !== 'undefined' && typeof req.query.pass !== 'undefined' && typeof req.query.email !== 'undefined') {
    UserController.Create(req.query.user, req.query.pass, req.query.email).then((resolve, reject) => {
      if (reject) { res.send('No way, Jose!'); }
      if (resolve) {
        res.send(resolve);
      } else {
        res.send('Failed to create user');
      }
    });
  } else {
    // At least one of our required fields are undefined
    res.send('FACK OFF M8');
  }
});


// Read User
// Rules for failed 'login' attempts:
// -> 3 attempts per cooldown
// -> Cooldowns: 1 minute, 5 minutes, 30 minutes, 24 hours
App.get('/readuser', (req, res) => {
  if (!Object.keys(userRequestThrottle).includes(req.ip)) {
    userRequestThrottle[req.ip] = { attempts: 0, blocked: false };
  }
  
  if(userRequestThrottle[req.ip].blocked !== true) {
    UserController.Read(req.query.user, req.query.pass).then((resolve, reject) => {
      if (reject) {
        console.log('REJECT!');
        res.sendStatus(400);
      }
      if (resolve === null || resolve === false || typeof resolve === 'undefined') {
        let num = ++userRequestThrottle[req.ip].attempts;
        if (num === 3) {
          userRequestThrottle[req.ip].blocked = true;
          console.log(`Blocked ${req.ip} for 1 minute`);
          setTimeout(() => {
            userRequestThrottle[req.ip].blocked = false;
            console.log(`Unblocked ${req.ip}`);
          }, 60000, req);
        } else if (num === 6) {
          userRequestThrottle[req.ip].blocked = true;
          console.log(`Blocked ${req.ip} for 5 minutes`);
          setTimeout(() => {
            userRequestThrottle[req.ip].blocked = false;
            console.log(`Unblocked ${req.ip}`);
          }, 300000, req);
        } else if (num === 9) {
          userRequestThrottle[req.ip].blocked = true;
          console.log(`Blocked ${req.ip} for 30 minutes`);
          setTimeout(() => {
            userRequestThrottle[req.ip].blocked = false;
            console.log(`Unblocked ${req.ip}`);
          }, 1800000, req);
        } else if (num === 12) {
          userRequestThrottle[req.ip].blocked = true;
          console.log(`Blocked ${req.ip} for 24 hours`);
          setTimeout(() => {
            userRequestThrottle[req.ip].blocked = false;
            console.log(`Unblocked ${req.ip}`);
          }, 86400000, req);
        }
        res.sendStatus(400);
      } else {
        res.send(resolve);
      }
    });
  } else {
    res.sendStatus(423);
  }
});
