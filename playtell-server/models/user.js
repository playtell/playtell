import mongoose from 'mongoose';
import crypto from 'crypto';
import settings from '../settings.json';

var UserModel;
var userSchema;

mongoose.connect('mongodb://localhost:27017/playtell', { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

async function settleConnection () {
  return db.once('open', function () {
    console.log('Mongoose connection open');
    userSchema = new mongoose.Schema({
      username: String,
      password: String,
      email: String
    });
    
    userSchema.methods.encryptPass = function (pass) {
      if (typeof pass === 'string') {
        const hash = crypto.createHmac(settings.encryptionSettings.method, settings.encryptionSettings.secret)
          .update(pass)
          .digest('hex');
        return hash;
      }
    };

    userSchema.methods.verifyPass = function (pass, input) { return input === userSchema.methods.encryptPass(pass) ? true : false }

    UserModel = mongoose.model('User', userSchema);
  });
}
settleConnection();

export { UserModel, userSchema, mongoose };
