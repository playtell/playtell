var express = require('express');

var App = express();

App.listen(3001, () => {
  console.log('web server listening on 3001');
});

App.use(express.static(__dirname + '/dist/'));

App.get('/', (req, res) => {
  res.sendFile(__dirname + '/dist/index.html');
});
