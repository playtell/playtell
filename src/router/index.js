import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/Home'
import Landing from '../pages/Landing'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    }
  ]
})
