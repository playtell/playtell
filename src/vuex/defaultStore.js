import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const defaultStore = new Vuex.Store({
  state: {
    loggedIn: false,
    shouldAuthModalShow: false,
    iconIDs: [
      '#icon-dpad',
      '#icon-graph',
      '#icon-keyboard',
      '#icon-controller',
      '#icon-mouse',
      '#icon-share',
      '#icon-thumbsdown',
      '#icon-thumbsup',
      '#icon-thumbsupdown',
      '#wifi',
      '#window',
      '#users2',
      '#user-ninja2',
      '#unlock-alt3',
      '#microchip',
      '#pen-alt',
      '#book2'
    ],
    pages: {
      'Browse': '/browse'
    },
    homeBlocks: [
      {
        'title': 'News for August',
        'author': 'Bunjomin',
        'date': '08/03/2018',
        'excerpt': 'Vestibulum ac dui sapien. Ut rutrum, metus quis interdum gravida, risus nisi dignissim nunc, at feugiat enim augue quis mi. Aliquam sed eros non magna commodo semper in a dui.',
        'link': {
          'url': '/',
          'label': 'Read More'
        }
      },
      {
        'title': 'Why God of War is my GOTY',
        'author': 'jerry420',
        'date': '08/02/2018',
        'excerpt': 'Vestibulum ac dui sapien. Ut rutrum, metus quis interdum gravida, risus nisi dignissim nunc, at feugiat enim augue quis mi. Aliquam sed eros non magna commodo semper in a dui.',
        'link': {
          'url': '/',
          'label': 'Read More'
        }
      },
      {
        'title': 'Fortnite or SNOREnite?',
        'author': 'alex_jones_4evr',
        'date': '08/01/2018',
        'excerpt': 'Vestibulum ac dui sapien. Ut rutrum, metus quis interdum gravida, risus nisi dignissim nunc, at feugiat enim augue quis mi. Aliquam sed eros non magna commodo semper in a dui.',
        'link': {
          'url': '/',
          'label': 'Read More'
        }
      },
      {
        'title': 'Top Review',
        'author': null,
        'date': null,
        'excerpt': 'Vestibulum ac dui sapien. Ut rutrum, metus quis interdum gravida, risus nisi dignissim nunc, at feugiat enim augue quis mi. Aliquam sed eros non magna commodo semper in a dui.',
        'link': {
          'url': '/',
          'label': 'Read More'
        }
      }
    ],
    recentActivity: [
      {
        'title': 'New 2D Indie Game!',
        'author': null,
        'content': 'You\'re not gonna believe this fuckin\' shit. Brand new indie dating sim made by one weeb over the course of 16 years.',
        'link': {
          'url': '/',
          'label': 'Read More'
        }
      },
      {
        'title': 'PUBG GOAT',
        'author': 'xX Dark Memer 69 Xx',
        'content': 'Ut rutrum, metus quis interdum gravida, risus nisi dignissim nunc, at feugiat enim augue quis mi. Aliquam sed eros non magna commodo semper in a dui.',
        'link': {
          'url': '/',
          'label': 'Full Review'
        }
      }
    ]
  },
  getters: {
    randomImages: (state) => {
      let min = Math.ceil(0)
      let max = Math.floor(state.iconIDs.length - 1)
      let returnArray = []
      for (let i = 0; i < 768; i++) {
        returnArray[i] = state.iconIDs[Math.floor(Math.random() * (max - min)) + min]
      }
      return returnArray
    },
    returnPages: (state) => {
      return state.pages
    },
    returnHomeBlocks: (state) => {
      return state.homeBlocks
    },
    returnRecentActivity: (state) => {
      return state.recentActivity
    },
    returnAuthModal: (state) => {
      return state.shouldAuthModalShow
    },
    returnUserAuthorized: (state) => {
      return state.loggedIn
    }
  },
  mutations: {
    toggleAuthModal: (state) => {
      state.shouldAuthModalShow = !state.shouldAuthModalShow
    },
    setUserLoggedInState: (state, newState) => {
      state.loggedIn = newState
    }
  }
})

export default defaultStore
